@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Welcome</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Welcome</li>
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    {{-- Main Content --}}
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h1>WebDev</h1>
                    {{-- <input type="checkbox" class="btn-check" id="btn-check" autocomplete="off" />
                    <label class="btn btn-primary" for="btn-check">Checkbox button</label> --}}
                    <button type="button" class="btn btn-block bg-gradient-primary btn-lg"><input type="checkbox" class="btn-check" id="btn-check" autocomplete="off" />Primary</button>
                </div>

                <div class="col-md-12">
                    <!-- Application buttons -->
                    <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">Application Buttons</h3>
                      </div>
                      <div class="card-body">
                        <p>Add the classes <code>.btn.btn-app</code> to an <code>&lt;a></code> tag to achieve the following:</p>
                        <a class="btn btn-app">
                          <i class="fas fa-edit"></i> Edit
                        </a>
                        <a class="btn btn-app">
                          <i class="fas fa-play"></i> Play
                        </a>
                        <a class="btn btn-app">
                          <i class="fas fa-pause"></i> Pause
                        </a>
                        <a class="btn btn-app">
                          <i class="fas fa-save"></i> Save
                        </a>
                        <a class="btn btn-app">
                          <span class="badge bg-warning">3</span>
                          <i class="fas fa-bullhorn"></i> Notifications
                        </a>
                        <a class="btn btn-app">
                          <span class="badge bg-success">300</span>
                          <i class="fas fa-barcode"></i> Products
                        </a>
                        <a class="btn btn-app">
                          <span class="badge bg-purple">891</span>
                          <i class="fas fa-users"></i> Users
                        </a>
                        <a class="btn btn-app">
                          <span class="badge bg-teal">67</span>
                          <i class="fas fa-inbox"></i> Orders
                        </a>
                        <a class="btn btn-app">
                          <span class="badge bg-info">12</span>
                          <i class="fas fa-envelope"></i> Inbox
                        </a>
                        <a class="btn btn-app">
                          <span class="badge bg-danger">531</span>
                          <i class="fas fa-heart"></i> Likes
                        </a>
                      </div>
                      <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>

            <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <button type="button" class="btn btn-block bg-gradient-info btn-lg">
              <div class="small-box bg-info">
                <div class="inner">
                  {{-- <h3>150</h3> --}}
  
                  <p>Blank</p>
                  
                </div>
                {{-- <div class="icon">
                  <i class="ion ion-bag"></i>
                </div> --}}
                {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
              </div>
              </button>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3>53<sup style="font-size: 20px">%</sup></h3>
  
                  <p>Bounce Rate</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3>44</h3>
  
                  <p>User Registrations</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h3>65</h3>
  
                  <p>Unique Visitors</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>
          <!-- /.row -->
        </div>
    </div>
    <!-- /.content -->

@endsection